package com.example.jinex.apigooglemaps;

import android.*;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jinex.apigooglemaps.Beans.user;
import com.example.jinex.apigooglemaps.DB.MyDatabase;
import com.gc.materialdesign.views.ButtonRectangle;

import java.io.File;

public class login extends AppCompatActivity {
    //Declaración de variables
    private MyDatabase db;
    private EditText email,pass;
    private ButtonRectangle login;
    private TextView tvtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Full Screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        //Asignacion de una variable para la BD
        db = new MyDatabase(login.this);
        //Obtener el path de la BD
        File database = getApplicationContext().getDatabasePath(MyDatabase.DBNAME);

        //Castear cajas de entrada
        email= (EditText) findViewById(R.id.txtEmail);
        pass= (EditText) findViewById(R.id.txtPass);
        //casteando botones
        login= (ButtonRectangle) findViewById(R.id.btnLogin);
        //Casteando texto
        tvtError= (TextView) findViewById(R.id.tvtError);
        //Solicitando permisos
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},0);


        //Declarando evento click del boton
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user usuario=new user(email.getText().toString(),pass.getText().toString());

                user  usuarioLogeado=db.login(usuario);

                if(usuarioLogeado!=null){
                    Intent a=new Intent(getApplicationContext(),puntos.class);
                    startActivity(a);
                    finish();
                }else{
                    tvtError.setText("Error de usuario o password");
                }
            }
        });


    }
}
