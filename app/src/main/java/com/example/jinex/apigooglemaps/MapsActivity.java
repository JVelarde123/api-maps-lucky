package com.example.jinex.apigooglemaps;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jinex.apigooglemaps.Modulos.DirectionFinder;
import com.example.jinex.apigooglemaps.Modulos.DirectionFinderListener;
import com.example.jinex.apigooglemaps.Modulos.Route;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapLongClickListener, DirectionFinderListener {
//Declaracion de variables
    private GoogleMap mMap;

    private double longitude;
    private double latitude;

    private double fromLongitude;
    private double fromLatitude;

    private int ini = 0, fin = 0;

    private double toLongitude;
    private double toLatitude;
    private String origin;
    private String destination;
    Polyline polyline;
    String transorte="";
    final CharSequence[]opciones={"Inicio","Destino"};
    private GoogleApiClient googleApiClient;

    private Marker inicio, destino;

    //extras para ruta//
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    //fin de los extras//

    FloatingActionButton button,bici,pie,compra;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps02);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        button = (FloatingActionButton) findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest("driving");
            }
        });

        bici= (FloatingActionButton) findViewById(R.id.bici);
        bici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest("bicycling");
            }
        });

        pie= (FloatingActionButton) findViewById(R.id.pie);
        pie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest("walking");
            }
        });

        compra= (FloatingActionButton) findViewById(R.id.compra);
        compra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle recupera = getIntent().getExtras();
                String resultado = recupera.getString("codigo");
                Intent a=new Intent(getApplicationContext(),compras.class);
                a.putExtra("codigo",resultado);
                startActivity(a);
            }
        });

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(AppIndex.API).build();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Asignar mapa
        mMap = googleMap;
        //Generar coordenada default por si no hay ningun marker
        LatLng latLng = new LatLng(-12.07625285497054, -77.05919981002808);
       //inicializar la linea
       PolylineOptions ab = new PolylineOptions().add(new LatLng(-12.07625285497054, -77.05919981002808)).add(new LatLng(-12.07625285497054, -77.05919981002808)).color(Color.parseColor("#f44336"));
        //asignar la linea al mapa pero invisible
        polyline = mMap.addPolyline(ab);

        polyline.setVisible(false);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));


        mMap.setOnMapLongClickListener(this);


        //botom mi localizacion
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setMyLocationEnabled(true);

            LocationManager locationManager = (LocationManager)
                    getSystemService(getApplicationContext().LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));;
            if (location != null) {
                //Getting longitude and latitude
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                //Para dibujar poligono
                fromLatitude=latitude;
                fromLongitude=longitude;
                Geocoder geo = new Geocoder(getApplicationContext());
                List<Address> direc = null;
                Address lugar = null;
                try {

                        direc = geo.getFromLocation(latitude, longitude, 1);
                        LatLng cord = new LatLng(latitude, longitude);
                        inicio = mMap.addMarker(new MarkerOptions().position(cord).draggable(true).visible(true).title("GPS"));
                        ini = 1;

                } catch (IOException e) {
                    e.printStackTrace();
                }
                lugar = direc.get(0);
                origin = lugar.getAddressLine(0).toString();
                //moving the map to location
                moveMap();
                }
            }else {
            Toast.makeText(getApplicationContext(),"Activar permisos de gps",Toast.LENGTH_LONG).show();
            }

        //fin del botom




            //Obtener datos
        Bundle recupera = getIntent().getExtras();
        String resultado = recupera.getString("codigo");

            Geocoder geo = new Geocoder(getApplicationContext());
            List<Address> direc = null;
            Address lugar = null;
            try {
                if (resultado.equals("totus")) {
                    direc = geo.getFromLocation(-12.0790589, -77.08777800000001, 1);
                    LatLng cord = new LatLng(-12.0790589, -77.08777800000001);
                    toLatitude=-12.0790589;
                    toLongitude=-77.08777800000001;
                    destino = mMap.addMarker(new MarkerOptions().position(cord).title("Totus La Marina").visible(true));
                    lugar = direc.get(0);
                    fin = 1;
                }else if(resultado.equals("metro")) {
                    direc = geo.getFromLocation(-12.0553988, -77.04210820000003, 1);
                    LatLng cord = new LatLng(-12.0553988, -77.04210820000003);
                    toLatitude=-12.0553988;
                    toLongitude=-77.04210820000003;
                    destino = mMap.addMarker(new MarkerOptions().position(cord).title("Metro alfonso Ugarte").visible(true));
                    lugar = direc.get(0);
                    fin = 1;
                }else {
                    direc = geo.getFromLocation(-12.1116549, -76.99102299999998, 1);
                    LatLng cord = new LatLng(-12.1116549, -76.99102299999998);
                    toLatitude=-12.1116549;
                    toLongitude=-76.99102299999998;
                    destino = mMap.addMarker(new MarkerOptions().position(cord).title("Wong Primavera").visible(true));
                    lugar = direc.get(0);
                    fin = 1;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            destination = lugar.getAddressLine(0).toString();


    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if(requestCode==0){
            if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);
            }
        }
    }

//Mover camara del mapa
    private void moveMap() {
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Mi posición"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    //metodo para generar url
    public String makeURL (){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/distancematrix/json");
        urlString.append("?origin=");// from
        urlString.append(origin);

        urlString.append("&destination=");// to
        urlString
                .append(destination);
        urlString.append("&sensor=false&mode=");
        urlString.append(transorte);
        urlString.append("&alternatives=true&waypoints=optimize:true");
        urlString.append("&key=AIzaSyAGQjITxwZ3GGkqylNRsThGAqRFUJJjGJ8");
        return urlString.toString();
    }

    //request con volley
    private void getDirection(){
        String url = makeURL();

        final ProgressDialog loading = ProgressDialog.show(this, "Obteniendo ruta", "Espere...", false, false);

        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();

                        drawPath(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //Metodo para medir distancia y pintarlo de color rojo
    public void drawPath(String  result) {

        LatLng from = new LatLng(fromLatitude,fromLongitude);
        LatLng to = new LatLng(toLatitude,toLongitude);


        Double distance = SphericalUtil.computeDistanceBetween(from, to);


        Toast.makeText(this,String.valueOf(distance+" Metros"),Toast.LENGTH_SHORT).show();


        try {
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(20)
                    .color(Color.RED)
                    .geodesic(true)
            );


        }
        catch (JSONException e) {

        }
    }

    //Metodo para leer coordenadas de ruta del MATRIX
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed( ConnectionResult connectionResult) {

    }





    public void finalOpe(View view){
        getDirection();
    }


    //Metodo para asignar marker de inicio y fin
    @Override
    public void onMapLongClick(final LatLng latLng) {

        //Cuadro de dialogo para definir actividad
        final AlertDialog.Builder alert=new AlertDialog.Builder(this);
        alert.setTitle("Seleccione la acción");

        alert.setSingleChoiceItems(opciones,-1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(opciones[which]=="Inicio"){
                    //inicio de Accion
                    //Clearing all the markers
                    if(inicio!=null) {
                        inicio.remove();
                    }
                    //Adding a new marker to the current pressed position
                    inicio=mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .draggable(true).visible(true));

                    latitude = latLng.latitude;
                    longitude = latLng.longitude;
                    if(fin==1 && ini==1){
                        polyline.remove();
                        PolylineOptions ab=new PolylineOptions().add(new LatLng(latitude,longitude)).add(new LatLng(toLatitude,toLongitude)).color(Color.parseColor("#f44336"));

                        polyline=mMap.addPolyline(ab);
                        polyline.setVisible(true);
                    }else{
                        ini=1;
                    }
                    Geocoder geocoder=new Geocoder(getApplicationContext());
                    List<Address> direccion=null;
                    Address lugar=null;
                    try {
                        direccion=geocoder.getFromLocation(latitude,longitude,1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(direccion!=null && direccion.size()>0){
                        lugar=direccion.get(0);
                    }

                    String complemento="";
                    try {
                        if (lugar.getAddressLine(0).toString()!=null){
                            complemento=lugar.getAddressLine(0).toString()+" ,";
                        }
                    }catch (NullPointerException e){
                        complemento="Lugar no valido";
                        origin="Lugar no valido";
                    }

                    try {
                        if(lugar.getLocality()!=null) {
                            complemento+=lugar.getLocality();
                            origin = lugar.getAddressLine(0).toString();
                        }
                    }catch (NullPointerException e){
                        complemento="Lugar no valido";
                    }


                    fromLatitude = latitude;
                    fromLongitude = longitude;
                    Toast.makeText(getApplicationContext(),"Punto de inicio "+complemento,Toast.LENGTH_SHORT).show();

                    //Fin de la Accion
                    dialog.dismiss();

                } if(opciones[which]=="Destino"){
                    //Inicio de la Accion
                    destino.remove();
                    //Adding a new marker to the current pressed position
                    destino=mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .draggable(true).visible(true));

                    latitude = latLng.latitude;
                    longitude = latLng.longitude;

                    if(ini==1){
                        polyline.remove();
                        PolylineOptions ab=new PolylineOptions().add(new LatLng(fromLatitude,fromLongitude)).add(new LatLng(latitude,longitude)).color(Color.parseColor("#f44336"));

                        polyline=mMap.addPolyline(ab);
                        polyline.setVisible(true);
                        fin=1;
                    }else{
                        fin=1;
                    }
                    Geocoder geocoder=new Geocoder(getApplicationContext());
                    List<Address> direccion=null;
                    Address lugar=null;
                    try {
                        direccion=geocoder.getFromLocation(latitude,longitude,1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(direccion!=null && direccion.size()>0){
                        lugar=direccion.get(0);
                    }

                    String complemento="";
                    try {
                        if (lugar.getAddressLine(0).toString()!=null){
                            complemento=lugar.getAddressLine(0).toString()+" ,";
                        }
                    }catch (NullPointerException e){
                        complemento="Lugar no valido";
                        destination="Lugar no valido";
                    }

                    try {
                        if(lugar.getLocality()!=null) {
                            complemento+=lugar.getLocality();
                            destination = lugar.getAddressLine(0).toString();
                        }
                    }catch (NullPointerException e){
                        complemento="Lugar no valido";
                    }




                    toLatitude = latitude;
                    toLongitude = longitude;
                    Toast.makeText(getApplicationContext(),"Destino "+complemento,Toast.LENGTH_SHORT).show();

                    //Fin de la Accion
                    dialog.dismiss();

                }
            }
        });
        alert.show();
    }

    //Metodo para ejecutar calculo de ruta con el Api MATRIX de google
    private void sendRequest(String dato) {
        String origen = origin;
        String destino = destination;
        transorte=dato;
        if(ini==0){
            Toast.makeText(this, "Seleccione un lugar de inicio", Toast.LENGTH_SHORT).show();
            return;
        }else  if (origen.equals("Lugar no valido")) {
            Toast.makeText(this, "Seleccione un lugar valido de inicio", Toast.LENGTH_SHORT).show();
            return;
        }else if(fin==0){
            Toast.makeText(this, "Seleccione un lugar de destino", Toast.LENGTH_SHORT).show();
            return;
        }else if (destino.equals("Lugar no valido")) {
            Toast.makeText(this, "Seleccione un lugar valido de destino", Toast.LENGTH_SHORT).show();
            return;
        }else {

            try {
                polyline.remove();
                new DirectionFinder(this, origen, destino).execute();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Un momento",
                "Buscando direccion", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    //Metodo para pintar el resultado del API de Google
    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        destino.remove();
        inicio.remove();

        for (Route route : routes) {

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));




            if(transorte.equals("driving")){
                Toast.makeText(this,"La distancia es: "+route.distance.text+" El tiempo que dura el viaje en carro es: "+route.duration.text,Toast.LENGTH_LONG).show();
            }
            if(transorte.equals("bicycling")){
                double tiempo=(Double.parseDouble(route.distance.value+"")/1000)*4;
                int tiempof=(int)tiempo;
                Toast.makeText(this,"La distancia es: "+route.distance.text+" El tiempo que dura el viaje en bicileta es: "+tiempof+" minutos.",Toast.LENGTH_LONG).show();
            }
            if(transorte.equals("walking")){
                double tiempo=(Double.parseDouble(route.distance.value+"")/1000)*10;
                int tiempof=(int)tiempo;
                Toast.makeText(this,"La distancia es: "+route.distance.text+" El tiempo que dura el viaje caminando: "+tiempof+" minutos.",Toast.LENGTH_LONG).show();
            }

        }

    }


}
