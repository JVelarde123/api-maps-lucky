package com.example.jinex.apigooglemaps;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jinex.apigooglemaps.Beans.productos;
import com.example.jinex.apigooglemaps.DB.MyDatabase;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;

public class compras extends AppCompatActivity {
    //Declaración de variables
    private MyDatabase db;
    ArrayList<productos> lstProductos=new ArrayList<productos>();
    String resultado ="";
    FloatingActionButton agregar,grabar;
    EditText nombre,nume;
    ArrayList<productos> lstnew=new ArrayList<productos>();
    int id=0;
    int val=0;
    RecyclerView contenedor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Full Screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_compras);

        //Asignacion de una variable para la BD
        db = new MyDatabase(compras.this);
        //Obtener el path de la BD
        File database = getApplicationContext().getDatabasePath(MyDatabase.DBNAME);

        //casteo de componentes
        contenedor= (RecyclerView) findViewById(R.id.contenedor2);
        nombre= (EditText) findViewById(R.id.txtNombre);
        nume= (EditText) findViewById(R.id.txtNum);
        agregar= (FloatingActionButton) findViewById(R.id.btnAgregar);
        grabar= (FloatingActionButton) findViewById(R.id.btnGrabar);

        //Recuperar datos de local
        Bundle recupera = getIntent().getExtras();
        resultado=recupera.getString("codigo");

        //Hacer Array de putos de venta
        lstProductos=db.listarP(resultado);
        id=lstProductos.size();

        //Adaptando el contenedor
        contenedor.setHasFixedSize(true);
        LinearLayoutManager layout=new LinearLayoutManager(getApplicationContext());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        contenedor.setAdapter(new adaptadorP(lstProductos));
        contenedor.setLayoutManager(layout);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombre.getText().toString().length()==0){
                    Toast.makeText(getApplicationContext(),"Campo Nombre vacio",Toast.LENGTH_LONG).show();
                }else if(nume.getText().toString().length()==0){
                    Toast.makeText(getApplicationContext(),"Campo Stock vacio",Toast.LENGTH_LONG).show();
                }else{
                    val=1;
                    id++;
                    productos aux=new productos(id,nombre.getText().toString(),Integer.parseInt(nume.getText().toString()));
                    lstProductos.add(aux);
                    lstnew.add(aux);
                    //Adaptando el contenedor
                    contenedor.setHasFixedSize(true);
                    LinearLayoutManager layout=new LinearLayoutManager(getApplicationContext());
                    layout.setOrientation(LinearLayoutManager.VERTICAL);
                    contenedor.setAdapter(new adaptadorP(lstProductos));
                    contenedor.setLayoutManager(layout);
                }
            }
        });

        grabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (val != 0) {
                    for (productos a : lstnew) {
                        db.insertProduc(a, resultado);
                    }
                    Intent a = new Intent(getApplicationContext(), puntos.class);
                    startActivity(a);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),"Ingrese una Compra minimo",Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
