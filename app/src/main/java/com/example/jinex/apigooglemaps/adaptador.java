package com.example.jinex.apigooglemaps;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jinex.apigooglemaps.Beans.puntosVentas;

import java.util.List;

/**
 * Created by POST on 19/01/2018.
 */
public class adaptador extends RecyclerView.Adapter<viewHolder> {

    List<puntosVentas> lstObjetos;

    public adaptador(List<puntosVentas> lstObjetos) {
        this.lstObjetos = lstObjetos;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,parent,false);
        return new viewHolder(vista);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        holder.titulo.setText(lstObjetos.get(position).getNombre());
        holder.imagen.setImageResource(lstObjetos.get(position).getImagen());
    }

    @Override
    public int getItemCount() {
        return lstObjetos.size();
    }
}
