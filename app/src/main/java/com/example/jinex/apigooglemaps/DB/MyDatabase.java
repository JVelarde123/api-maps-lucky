package com.example.jinex.apigooglemaps.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.jinex.apigooglemaps.Beans.productos;
import com.example.jinex.apigooglemaps.Beans.user;

import java.util.ArrayList;


/**
 * Created by Jinex on 28/03/2017.
 */
public class MyDatabase extends SQLiteOpenHelper {

    public static final String DBNAME = "lucky.db";
    public static final String DBLOCATION = "/data/data/com.example.jinex.apigooglemaps/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public MyDatabase(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if(mDatabase!=null) {
            mDatabase.close();
        }
    }

    //Metodo para logear
    public user login(user usuin) throws SQLException{
        user usuario=null;
        openDatabase();
        Cursor cursor=null;
        cursor = mDatabase.rawQuery("SELECT * FROM user where mail='"+usuin.getEmail()+"' and pass='"+usuin.getPass()+"';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            usuario= new user(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return usuario;
    }

    //Metodo para listar Compras segun local
    public ArrayList<productos>listarP(String place) throws SQLException{
        productos a=null;
        ArrayList<productos> item= new ArrayList<productos>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM produc where place='"+place+"' ORDER BY _id ASC;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            a=new productos(cursor.getInt(0),cursor.getString(1),cursor.getInt(2));
            item.add(a);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return item;
    }

    //Metodo para insertar productos
    public void insertProduc(productos p, String place){
        openDatabase();
        String sql="insert into produc values(null,'"+p.getNombre()+"','"+p.getStock()+"','"+place+"');";
        try{
            mDatabase.execSQL(sql);
        }catch (Exception e){}
        closeDatabase();
    }


}

