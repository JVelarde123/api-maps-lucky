package com.example.jinex.apigooglemaps.Beans;

/**
 * Created by POST on 19/01/2018.
 */
public class puntosVentas {
    //Declaración de variables
    private String nombre;
    private int imagen;

    //Constructor

    public puntosVentas(int imagen, String nombre) {
        this.imagen = imagen;
        this.nombre = nombre;
    }

    //Getters & Setters

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
