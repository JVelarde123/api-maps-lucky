package com.example.jinex.apigooglemaps.Beans;

/**
 * Created by POST on 21/01/2018.
 */
public class productos {
    //Declaracion de variables
    private int id,stock;
    private String nombre;
    //constructor

    public productos(int id, String nombre, int stock) {
        this.id = id;
        this.nombre = nombre;
        this.stock = stock;
    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
