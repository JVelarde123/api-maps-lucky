package com.example.jinex.apigooglemaps.Beans;

/**
 * Created by POST on 18/01/2018.
 */
public class user {
    //declaración de variables
    private String email,pass;

    //Constructor
    public user(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    //Constructor con 1 dato
    public user(String email) {
        this.email = email;
    }

    //Getters & Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
