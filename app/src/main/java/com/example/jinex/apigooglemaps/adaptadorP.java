package com.example.jinex.apigooglemaps;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jinex.apigooglemaps.Beans.productos;

import java.util.List;

/**
 * Created by POST on 21/01/2018.
 */
public class adaptadorP extends RecyclerView.Adapter<viewHolder2> {

    List<productos> lstObjetos;

    public adaptadorP(List<productos> lstObjetos) {
        this.lstObjetos = lstObjetos;
    }

    @Override
    public viewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_compra,parent,false);
        return new viewHolder2(vista);
    }

    @Override
    public void onBindViewHolder(viewHolder2 holder, int position) {
        holder.nombre.setText(lstObjetos.get(position).getNombre());
        holder.id.setText("ID: "+lstObjetos.get(position).getId());
        holder.stock.setText("Stock: "+lstObjetos.get(position).getStock());
    }

    @Override
    public int getItemCount() {
        return lstObjetos.size();
    }
}