package com.example.jinex.apigooglemaps;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;

import com.example.jinex.apigooglemaps.Beans.puntosVentas;

import java.util.ArrayList;

public class puntos extends AppCompatActivity {

    LocationManager locationManager;
    AlertDialog alert = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Full Screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_puntos);

        //Hacer Array de putos de venta
        ArrayList<puntosVentas> lstVenta=new ArrayList<puntosVentas>();
        puntosVentas a=new puntosVentas(R.drawable.totus,"Totus la Marina");
        puntosVentas b=new puntosVentas(R.drawable.metro,"Metro Ugarte");
        puntosVentas c=new puntosVentas(R.drawable.wong,"Wong Primavera");
        lstVenta.add(a);
        lstVenta.add(b);
        lstVenta.add(c);

        //casteo del recycle
        RecyclerView contenedor= (RecyclerView) findViewById(R.id.contenedor);
        //Adaptando el contenedor
        contenedor.setHasFixedSize(true);
        LinearLayoutManager layout=new LinearLayoutManager(getApplicationContext());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        contenedor.setAdapter(new adaptador(lstVenta));
        contenedor.setLayoutManager(layout);


        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        /****metodo para activar GPS****/
        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertNoGps();
        }
        /********/
    }

    //metodo para ver si esta activado el gps
    private void AlertNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El sistema GPS esta desactivado, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }


}
