package com.example.jinex.apigooglemaps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Created by POST on 19/01/2018.
 */
public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    //declaracion de controles
    Button btnUno,btnDos;
    ImageView imagen;
    TextView titulo;


    public viewHolder(View itemView) {
        super(itemView);

        //Casteo de controles
        btnUno= (Button) itemView.findViewById(R.id.btnUno);
        btnDos= (Button) itemView.findViewById(R.id.btnDos);
        imagen= (ImageView) itemView.findViewById(R.id.imagen);
        titulo= (TextView) itemView.findViewById(R.id.texto);

        btnUno.setOnClickListener(this);
        btnDos.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        //Obtener posicion del Recycle
        int position=getAdapterPosition();
        //Ver boton pulsado
        if(view.getId()== btnUno.getId()){
            if(position==0){
                Intent intent = new Intent( view.getContext(),MapsActivity.class);
                String nombre="totus";
                intent.putExtra("codigo",nombre);
                view.getContext().startActivity(intent);
            }else if(position==1){
                Intent intent = new Intent( view.getContext(),MapsActivity.class);
                String nombre="metro";
                intent.putExtra("codigo",nombre);
                view.getContext().startActivity(intent);
            }else{
                Intent intent = new Intent( view.getContext(),MapsActivity.class);
                String nombre="wong";
                intent.putExtra("codigo",nombre);
                view.getContext().startActivity(intent);
            }
        }else {
            if(position==0){
                Uri uri = Uri.parse("http://www.tottus.com.pe/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                view.getContext().startActivity(intent);
            }else if(position==1){
                Uri uri = Uri.parse("https://www.metro.pe/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                view.getContext().startActivity(intent);
            }else{
                Uri uri = Uri.parse("https://www.wong.pe/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                view.getContext().startActivity(intent);
            }

        }

    }
}
